<?php
header("Content-type: application/json; charset=utf-8");
$subject  = "Contato via website: " . $_POST["radio"];
$mailFrom = trim($_POST["email"]);
$name     = trim($_POST["name"]);
$qua  = trim($_POST["qua"]);
$dv  = trim($_POST["dv"]);
$cas  = trim($_POST["cas"]);
$mar  = trim($_POST["mar"]);
$nc  = trim($_POST["nc"]);
if ($name != null && $mailFrom != null && $qua != null && $dv != null) {
	if($cas != null || $mar != null && $nc != null)	{
		if (!filter_var($mailFrom, FILTER_VALIDATE_EMAIL) === false) {
			$mailTo  = "sociallab018@gmail.com"; /*Email que irá receber */
			$headers = "Enviado por: " . $mailFrom;
			$txt     = "Solicitação de " . $_POST["radio"] . "\n\n" . "Nome/Fórmula: " . $name . "\n\n" . "Quantidade: " . $qua . "\n\n" . "Data de Validade: " . $dv . "\n\n" . "CAS Number: " . $cas . "\n\n" . "Marca: " . $mar . "\n\n" . "Número de Catálogo: " . $nc;
			mail($mailTo, $subject, $txt, $headers);
			$signal = "ok";
			$msg    = "Enviado";
		} else {
			$signal = "bad";
			$msg    = "Email inválido.";
		}
	} else {
		$signal = "bad";
		$msg    = "Por favor preencha todos os campos.";
	}
	
} else {
    $signal = "bad";
    $msg    = "Por favor preencha todos os campos.";
}
$data = array(
    'signal' => $signal,
	'msg' => $msg
);
echo json_encode($data);
?>
